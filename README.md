# Ding.js

> An Internet of Things framework

<!-- [![NPM Version][npm-image]][npm-url]
[![Build Status][travis-image]][travis-url]
[![Downloads Stats][npm-downloads]][npm-url] -->

<!-- One to two paragraph statement about your product and what it does. -->

<!-- ## Installation

```sh
npm install my-crazy-module --save
``` -->

<!-- ## Usage example

A few motivating and useful examples of how your product can be used. Spice this up with code blocks and potentially more screenshots.

_For more examples and usage, please refer to the [Wiki][wiki]._ -->

## Development setup

Development can be done using Docker and docker-compose or using an npm run script. The default configuration sets up an development environment. The source files are automatically monitored for any changes. The project is then compiled, and the server is automatically restarted.

Start developing using docker-compose:

```sh
docker-compose up
```

If you need to rebuild the container, just use:

```sh
docker-compose up --build
```

Start developing without docker:

```sh
npm install
npm run dev
```

## Contributing

1. Fork it (https://gitlab.com/devbit/dingjs)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request